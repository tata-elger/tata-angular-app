'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'ngResource',
  'ngAnimate',
  'myApp.controllers',
  'myApp.filters',
  'myApp.directives',
  'myApp.restService',
  'angular-loading-bar',
  'toaster',
  'LocalStorageModule'
])
.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/javascript', { templateUrl: 'views/javascript.html', controller: 'view1Controller' });
    $routeProvider.when('/bootstrapshow', { templateUrl: 'views/bootstrapshow.html', controller: 'view2Controller' });
    $routeProvider.when('/lotsofthings', { templateUrl: 'views/lotsofthings.html', controller: 'lotsofthingsController' });
    $routeProvider.otherwise({ redirectTo: '/bootstrapshow' });
}]);

