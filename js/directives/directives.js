'use strict';

/* Chart Directives */

angular.module('myApp.directives', [])
    .directive('navBar', function () {
        return {
            restrict: 'E',
            templateUrl: 'directives/navbar.html',
            link: function ($scope, elm, attrs) {
                $scope.testData = "Haal bijvoorbeeld dynamisch menu items op uit een service";
            },
            controller: function ($scope, $location) {

                $scope.getUrl = function (url) {

                    return $location.url() === url;

                }

            }
        }
    })
    .directive('directiveVoorbeeld', function () {
        return {
            restrict: 'E',
            templateUrl: 'directives/directive-voorbeeld.html',
            link: function ($scope, elm, attrs) {
                $scope.testData = "Dit komt uit een directive";
            }
        }
    })
;