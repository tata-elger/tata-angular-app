'use strict';

angular.module('myApp.controllers', [])
  .controller('view1Controller', function ($scope, toaster) {
      $scope.dit = 300;
      $scope.dat = 400;

      $scope.showSuccess = function () {
          toaster.pop('success', "Success popup!", "Dit is een success");
      }
      $scope.showWarning = function () {
          toaster.pop('warning', "Waarshuwing", "waarschuwing tekst");
      }
      $scope.showError = function () {
          toaster.pop('error', "Hij doet het niet :(", "Extra toelichting waarom");
      }

      $('#accordion').collapse();
  })
  .controller('view2Controller', function ($scope) {
      $('.carousel').carousel();
  })
  .controller('view3Controller', function ($scope, Car) {
    $scope.car = Car.get({ id: 1 });
    $scope.cars = Car.query();

    $scope.update = function () {
        Car.save($scope.car).$promise
        .then(function () {
            $scope.messages = "success";
        }).catch(function(result) {
            $scope.messages = result.data;
        });
    }
})
  .controller('lotsofthingsController', function ($scope) {
      $scope.thing = 1234;
  })
;