'use strict';
var app = angular.module('myApp.restService', ['ngResource']);

/* 
* Voorbeeld connectie naar een rest service
* - hiervoor is angular ngResource nodig
* - maakt verbinding met pc: ijmev12engict01 waar een voorbeeld rest api draait
* - dmv dependency injection kan je Car in elke controller/directive/filter injecteren (zie controllers.js voor een voorbeeld)
* - dit werkt zolang de ijmev12engict01 pc het doet 
*/

app.factory("Car", function ($resource) {
    return $resource("http://ijmev12engict01/api/car");
});

/*
* interceptor, voor authenticatie en als er geen verbinding is:
*/

app.factory('interceptorService', ['$q', '$location', 'localStorageService', 'toaster', function ($q, $location, localStorageService, toaster) {
    var authInterceptorServiceFactory = {};

    authInterceptorServiceFactory.request = function (config) {
        config.headers = config.headers || {};
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }
        return config;
    }

    authInterceptorServiceFactory.requestError = function (rejection) {
        //toaster.pop('error', "Geen verbinding", "Geen verbinding :(");
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.responseError = function (rejection) {
        if (rejection.status === 401) {
            $location.path('/login');
        } else {
            toaster.pop('error', "Geen verbinding", "Geen verbinding :(");
        }
        return $q.reject(rejection);
    };

    return authInterceptorServiceFactory;
}]);

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('interceptorService');
}]);
